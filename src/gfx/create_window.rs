#[path = "window_events.rs"] mod window_events;

use std::sync::Arc;
use futures_micro::yield_once;
use vulkano::{swapchain::{Surface, Swapchain, SwapchainCreateInfo, SwapchainCreationError}, image::{ImageUsage, SwapchainImage}, pipeline::graphics::viewport::Viewport};
use xcb::{Connection, x::{self, Atom}, Xid};
use crate::util::box_err::BoxErr;
pub use window_events::WindowEvent;

use super::vulkano_context::VulkanoContext;

pub trait Window
{
	fn next_event(&mut self, vulkano: &VulkanoContext) -> Result<Option<WindowEvent>, BoxErr>;
	fn close(&self) -> Result<(), BoxErr>;
	fn drop(&mut self) {self.close().expect("Unable to close window after it lost scope.");}
	fn surface(&self) -> &Surface;
	fn viewport(&self) -> Viewport;
	fn swapchain(&self) -> Arc<Swapchain>;
	fn swapchain_images(&self) -> &[Arc<SwapchainImage>];
}

pub async fn open_window_auto(window_name: &str, vulkano: &VulkanoContext) -> Result<Box<dyn Window>, BoxErr>
{
	//TODO: Compile-time platform dynamism.
	Ok(Box::new(open_x11_window(window_name, vulkano).await?))
}

pub struct XCBWindow
{
	connection: Arc<Connection>,
	window: x::Window,
	atom_delete_window: Atom,
	surface: Arc<Surface>,
	swapchain: Option<Arc<Swapchain>>,
	swapchain_images: Option<Vec<Arc<SwapchainImage>>>,
	expose_dims: [u16; 4],
}

pub async fn open_x11_window(window_name: &str, vulkano: &VulkanoContext) -> Result<XCBWindow, BoxErr>
{
	let (connection, screen_num) = Connection::connect(None)?;
	let connection = Arc::new(connection);
	let setup = connection.get_setup();
	let screen = setup.roots().nth(screen_num.try_into().unwrap()).unwrap();
	let window: x::Window = connection.generate_id();

	connection.check_request(connection.send_request_checked(&x::CreateWindow
	{
		depth: x::COPY_FROM_PARENT as u8,
		wid: window,
		parent: screen.root(),
		x: 0,
		y: 0,
		width: screen.width_in_pixels(),
		height: screen.height_in_pixels(),
		border_width: 0,
		class: x::WindowClass::InputOutput,
		visual: screen.root_visual(),
		value_list:
		&[
			x::Cw::BackPixel(screen.white_pixel()),
			x::Cw::EventMask(x::EventMask::EXPOSURE | x::EventMask::KEY_PRESS)
		],
	}))?;

	connection.check_request(connection.send_request_checked(&x::ChangeProperty
	{
		mode: x::PropMode::Replace,
		window,
		property: x::ATOM_WM_NAME,
		r#type: x::ATOM_STRING,
		data: window_name.to_string().as_bytes(),
	}))?;

	connection.send_request(&x::MapWindow {window});

	let cookies =
	(
		connection.send_request(&x::InternAtom
		{
			only_if_exists: true,
			name: b"WM_PROTOCOLS",
		}),
		connection.send_request(&x::InternAtom {
			only_if_exists: true,
			name: b"WM_DELETE_WINDOW",
		}),
	);
	let atom_protocols = connection.wait_for_reply(cookies.0)?.atom();
	let atom_delete_window = connection.wait_for_reply(cookies.1)?.atom();

	connection.check_request(connection.send_request_checked(&x::ChangeProperty
	{
		mode: x::PropMode::Replace,
		window,
		property: atom_protocols,
		r#type: x::ATOM_ATOM,
		data: &[atom_delete_window],
	}))?;

	let surface = unsafe
	{
		Surface::from_xcb(vulkano.instance.clone(), connection.get_raw_conn(), window.resource_id(), Some(connection.clone()))?
	};

	let mut window = XCBWindow
	{
		connection,
		window,
		atom_delete_window,
		surface,
		swapchain: None,
		swapchain_images: None,
		expose_dims: [0; 4],
	};

	loop
	{
		let event = window.next_event(&vulkano)?;
		if event.is_some()
		{
			let event = event.unwrap();
			match event
			{
				WindowEvent::Expose(_) => return Ok(window),
				event =>
				{
					println!("WARNING: Dropped window event due to not being exposed: {:?}", event);
				}
			}
		}
		yield_once().await;
	}
}

impl XCBWindow
{
	fn try_create_swapchain(&mut self, vulkano: &VulkanoContext, width: u16, height: u16) -> Result<bool, BoxErr>
	{
		let surface_caps = vulkano.physical_device.surface_capabilities(&self.surface, Default::default())?;
		let composite_alpha = surface_caps
			.supported_composite_alpha
			.into_iter()
			.next()
			.ok_or("Failed to get composite alpha from surface.")?;
		let mut surface_formats = vulkano
			.physical_device
			.surface_formats(&self.surface, Default::default())?;
		let (image_format, _image_color_space) = match surface_formats.first()
		{
			Some(_) => Ok(surface_formats.swap_remove(0)),
			None => Err("Failed to query surface image format.")
		}?;
		let swapchain_info = SwapchainCreateInfo
		{
			min_image_count: surface_caps.min_image_count + 1,
			image_format: Some(image_format),
			image_extent: [width.try_into()?, height.try_into()?],
			image_usage: ImageUsage::COLOR_ATTACHMENT,
			composite_alpha,
			..Default::default()
		};
		let (swapchain, swapchain_images) = match if self.swapchain.is_some()
		{
			self.swapchain.as_ref().unwrap().recreate(swapchain_info)
		}
		else
		{
			Swapchain::new
			(
				vulkano.device.clone(),
				self.surface.clone(),
				swapchain_info,
			)
		}
		{
			Err(SwapchainCreationError::ImageExtentNotSupported
			{
				provided: _,
				min_supported: _,
				max_supported: _
			}) => return Ok(false),
			other => other,
		}?;
		self.swapchain = Some(swapchain);
		self.swapchain_images = Some(swapchain_images);
		Ok(true)
	}
}

impl Window for XCBWindow
{
	fn next_event(&mut self, vulkano: &VulkanoContext) -> Result<Option<WindowEvent>, BoxErr>
	{
		let next = self.connection.poll_for_event()?;
		if next.is_none() {return Ok(None);}
		Ok(match next.unwrap()
		{
			xcb::Event::X(x::Event::KeyPress(event)) => {Some(WindowEvent::Keypress(event.detail()))},
			xcb::Event::X(x::Event::ClientMessage(ev)) =>
			{
				if let x::ClientMessageData::Data32([atom, ..]) = ev.data()
				{
					if atom == self.atom_delete_window.resource_id() {Some(WindowEvent::Close)}
					else {None}
				}
				else {None}
			},
			xcb::Event::X(x::Event::Expose(ev)) =>
			{
				let width = ev.width();
				let height = ev.height();
				if width > 1 && height > 1
				{
					let swapchain_created = self.try_create_swapchain(vulkano, width, height)?;
					self.expose_dims = [ev.x(), ev.y(), width, height];
					if swapchain_created {Some(WindowEvent::Expose(self.expose_dims))}
					else {None}
				}
				else {None}
			},
			_ => None,
		})
	}

	fn surface(&self) -> &Surface {&self.surface}

	fn close(&self) -> Result<(), BoxErr>
	{
		Ok(self.connection.check_request(self.connection.send_request_checked(&x::DestroyWindow
		{
			window: self.window,
		}))?)
	}

	fn viewport(&self) -> Viewport
	{
		let [width, height] = [self.expose_dims[2] as f32, self.expose_dims[3] as f32];
		Viewport
		{
			origin: [0.0, 0.0],
			dimensions: [width, height],
			depth_range: 0.0..1.0
		}
	}

	fn swapchain(&self) -> Arc<Swapchain> {self.swapchain.as_ref().unwrap().clone()}

	fn swapchain_images(&self) -> &[Arc<SwapchainImage>]
	{
		&(self.swapchain_images.as_ref().unwrap())[..]
	}
}