#![allow(dead_code)]

use crate::{math::vulkano_vectors::{Vector3D, VulkanoVectorIntegral}, gfx::shaders::vertex::Vertex};

pub type VulkanoTriangleDefinitionIntegral = u32;

pub struct TriangleMesh
{
	points: Vec<Vector3D>,
	triangles: Vec<(VulkanoTriangleDefinitionIntegral, VulkanoTriangleDefinitionIntegral, VulkanoTriangleDefinitionIntegral)>,
}

impl TriangleMesh
{
	pub fn to_vertices(&self) -> Vec<Vertex>
	{
		let color = Vector3D {position: [1.0, 1.0, 1.0]};
		self.triangles.iter().flat_map(|tri|
		{
			let (first, second, third) = tri;
			let first = self.points.get((*first) as usize).unwrap();
			let second = self.points.get((*second) as usize).unwrap();
			let third = self.points.get((*third) as usize).unwrap();
			let delta_a = *second.as_vectord() - *first.as_vectord();
			let delta_b = *third.as_vectord() - *first.as_vectord();
			let (normal, _) = delta_a.cross_product(delta_b).normalized();
			let normal = *normal.as_vector3d();
			vec!
			(
				Vertex
				{
					position: *first,
					color,
					normal,
				},
				Vertex
				{
					position: *second,
					color,
					normal,
				},
				Vertex
				{
					position: *third,
					color,
					normal,
				},
			)
		}).collect()
	}
	pub fn cube(width: VulkanoVectorIntegral) -> TriangleMesh
	{
		let mut points = [Vector3D::empty(); 8];
		for x in 0..2 as usize
		{
			for y in 0..2 as usize
			{
				for z in 0..2 as usize
				{
					let index = x + (y<<1) + (z<<2);
					let x = (x as VulkanoVectorIntegral) * width - (width / 2.0);
					let y = (y as VulkanoVectorIntegral) * width - (width / 2.0);
					let z = (z as VulkanoVectorIntegral) * width - (width / 2.0);
					let vec = Vector3D{position: [x, y, z]};
					points[index] = vec;
				}
			}
		}
		let triangles =
		[
			// Bottom
			(0, 2, 1),
			(1, 2, 3),
			// Top
			(4, 5, 6),
			(6, 5, 7),
			// Left
			(0, 1, 4),
			(4, 1, 5),
			// Right
			(2, 6, 3),
			(3, 6, 7),
			// Back
			(0, 4, 2),
			(2, 4, 6),
			// Front
			(1, 3, 5),
			(5, 3, 7),
		];
		TriangleMesh
		{
			points: Vec::from(points),
			triangles: Vec::from(triangles),
		}
	}
}