use std::sync::Arc;
use vulkano::command_buffer::{AutoCommandBufferBuilder, CommandBufferUsage, RenderPassBeginInfo, SubpassContents, PrimaryAutoCommandBuffer};
use vulkano::format::Format;
use vulkano::image::{ImageAccess, AttachmentImage};
use vulkano::pipeline::PipelineBindPoint;
use vulkano::pipeline::graphics::depth_stencil::DepthStencilState;
use vulkano::pipeline::graphics::rasterization::{CullMode, RasterizationState};
use vulkano::pipeline::{GraphicsPipeline, graphics::{vertex_input::Vertex, viewport::ViewportState, input_assembly::InputAssemblyState}, Pipeline};
use vulkano::render_pass::{Subpass, RenderPass, Framebuffer, FramebufferCreateInfo};
use vulkano::image::{SwapchainImage, view::ImageView};
use vulkano::device::{Device, Queue};
use vulkano::sync::{self, GpuFuture};
use vulkano::swapchain::{Swapchain, acquire_next_image, AcquireError, SwapchainPresentInfo};
use vulkano::descriptor_set::{PersistentDescriptorSet, WriteDescriptorSet};
use vulkano::buffer::{Buffer, BufferCreateInfo, BufferUsage, Subbuffer};
use vulkano::memory::allocator::{AllocationCreateInfo, MemoryUsage, GenericMemoryAllocator, Suballocator};
use crate::util::box_err::BoxErr;
use super::shaders::scene_transform::{SceneTransform, generate_view_transform, generate_projection_transform, generate_correction_transform};
use super::shaders::standard_push_constant::StandardPushConstant;
use super::shaders::standard_shaders;
use super::shaders::standard_uniform_data::{StandardVertexUniformData, StandardLightingUniformData};
use super::{vulkano_context::VulkanoContext, create_window::Window, scene::scene::Scene};

const DEPTH_BUFFER_FORMAT: Format = Format::D16_UNORM;
const COLOR_BUFFER_FORMAT: Format = Format::A2B10G10R10_UNORM_PACK32;
const NORMAL_BUFFER_FORMAT: Format = Format::R16G16B16A16_SFLOAT;

fn generate_render_pass(device: Arc<Device>, window: &dyn Window) -> Result<Arc<RenderPass>, BoxErr>
{
	Ok(vulkano::ordered_passes_renderpass!
	(
		device.clone(),
		attachments:
		{
			final_color:
			{
				load: Clear,
				store: Store,
				format: window.swapchain().image_format(),
				samples: 1,
			},
			color:
			{
				load: Clear,
				store: DontCare,
				format: COLOR_BUFFER_FORMAT,
				samples: 1,
			},
			normals:
			{
				load: Clear,
				store: DontCare,
				format: NORMAL_BUFFER_FORMAT,
				samples: 1,
			},
			depth:
			{
				load: Clear,
				store: DontCare,
				format: DEPTH_BUFFER_FORMAT,
				samples: 1,
			},
		},
		passes:
		[
			{
				color: [color, normals],
				depth_stencil: {depth},
				input: [],
			},
			{
				color: [final_color],
				depth_stencil: {},
				input: [color, normals],
			}
		]
	)?)
}

fn generate_framebuffers_from_swapchain<T: Suballocator>(images: &[Arc<SwapchainImage>], render_pass: Arc<RenderPass>, allocator: &GenericMemoryAllocator<T>) -> Result<(Vec<Arc<Framebuffer>>, Arc<ImageView<AttachmentImage>>, Arc<ImageView<AttachmentImage>>), BoxErr>
{
	let dimensions = images[0].dimensions().width_height();
	let depth_buffer = ImageView::new_default
	(
		AttachmentImage::transient(allocator, dimensions, DEPTH_BUFFER_FORMAT)?
	)?;
	let color_buffer = ImageView::new_default
	(
		AttachmentImage::transient_input_attachment(allocator, dimensions, COLOR_BUFFER_FORMAT)?
	)?;
	let normal_buffer = ImageView::new_default
	(
		AttachmentImage::transient_input_attachment(allocator, dimensions, NORMAL_BUFFER_FORMAT)?,
	)?;

	let results: Result<Vec<Arc<Framebuffer>>, _> = images.iter().map(|image| -> Result<Arc<Framebuffer>, BoxErr>
	{
		let view = ImageView::new_default(image.clone())?;
		Ok(Framebuffer::new
		(
			render_pass.clone(),
			FramebufferCreateInfo
			{
				attachments: vec!
				[
					view,
					color_buffer.clone(),
					normal_buffer.clone(),
					depth_buffer.clone()
				],
				..Default::default()
			},
		)?)
	}).collect();
	Ok((results?, color_buffer, normal_buffer))
}

fn generate_render_cycle_command_buffer(vulkano: &VulkanoContext, render_pipeline: &RenderPipeline, framebuffer: Arc<Framebuffer>, scene: &Scene) -> Result<Arc<PrimaryAutoCommandBuffer>, BoxErr>
{
	let &VulkanoContext
	{
		queue_family_index,
		ref command_buffer_allocator,
		..
	} = vulkano;
	let &RenderPipeline
	{
		ref pipelines,
		ref uniform_descriptor_sets,
		..
	} = render_pipeline;

	let mut builder = AutoCommandBufferBuilder::primary
	(
		command_buffer_allocator,
		queue_family_index,
		CommandBufferUsage::MultipleSubmit
	)?;
	let depth_clear = 1.0;
	let color_clear = [0.0,0.0,0.0,1.0];
	let normal_clear = [0.0,0.0,0.0,1.0];
	builder
		.begin_render_pass
		(
			RenderPassBeginInfo
			{
				clear_values: vec!
				[
					Some(color_clear.into()),
					Some(color_clear.into()),
					Some(normal_clear.into()),
					Some(depth_clear.into()),
				],
				..RenderPassBeginInfo::framebuffer(framebuffer.clone())
			},
			SubpassContents::Inline
		)?
		.bind_pipeline_graphics(pipelines[0].clone())
		.bind_descriptor_sets
		(
			PipelineBindPoint::Graphics,
			pipelines[0].layout().clone(),
			0,
			uniform_descriptor_sets[0].clone(),
		);
	let results: Result<Vec<_>, _> = scene.models.iter().map(|(vbo, placement)|
	{
		let (model_transform, normal_transform) = placement.generate_model_transforms();
		builder.push_constants
		(
			pipelines[0].layout().clone(),
			0,
			StandardPushConstant
			{
				model_transform,
				normal_transform,
				..Default::default()
			},
		);
		vbo.draw_vbo_operation(&mut builder)
	}).collect();
	results?;
	builder
		.next_subpass(SubpassContents::Inline)?
		.bind_pipeline_graphics(pipelines[1].clone())
		.bind_descriptor_sets
		(
			PipelineBindPoint::Graphics,
			pipelines[1].layout().clone(),
			0,
			uniform_descriptor_sets[1].clone(),
		);
	let results: Result<Vec<_>, _> = scene.models.iter().map(|(vbo, placement)|
	{
		let (model_transform, normal_transform) = placement.generate_model_transforms();
		builder.push_constants
		(
			pipelines[0].layout().clone(),
			0,
			StandardPushConstant
			{
				model_transform,
				normal_transform,
				..Default::default()
			},
		);
		vbo.draw_vbo_operation(&mut builder)
	}).collect();
	results?;
	builder.end_render_pass()?;
	let command_buffer = Arc::new(builder.build()?);
	Ok(command_buffer)
}

fn setup_uniform_layout(vulkano: &VulkanoContext, pipelines: &[Arc<GraphicsPipeline>], attachment_buffers: &[Arc<ImageView<AttachmentImage>>]) -> Result<(Subbuffer<StandardVertexUniformData>, Subbuffer<StandardLightingUniformData>, Vec<Arc<PersistentDescriptorSet>>), BoxErr>
{
	let &VulkanoContext
	{
		ref default_allocator,
		ref descriptor_set_allocator,
		..
	} = vulkano;

	let uniform_vertex_buffer = Buffer::from_data
	(
		default_allocator,
		BufferCreateInfo {usage: BufferUsage::UNIFORM_BUFFER, ..Default::default()},
		AllocationCreateInfo {usage: MemoryUsage::Upload, ..Default::default()},
		StandardVertexUniformData::default()
	)?;
	let uniform_lighting_buffer = Buffer::from_data
	(
		default_allocator,
		BufferCreateInfo {usage: BufferUsage::UNIFORM_BUFFER, ..Default::default()},
		AllocationCreateInfo {usage: MemoryUsage::Upload, ..Default::default()},
		StandardLightingUniformData::default()
	)?;

	let descriptor_set_layout = pipelines[0].layout().set_layouts().get(0)
		.ok_or("Could not get deferred pipeline layout.")?;
	let uniform_descriptor_set = PersistentDescriptorSet::new
	(
		descriptor_set_allocator,
		descriptor_set_layout.clone(),
		[
			WriteDescriptorSet::buffer(0, uniform_vertex_buffer.clone()),
		],
	)?;

	let lighting_layout = pipelines[1].layout().set_layouts().get(0)
		.ok_or("Could not get lighting pipeline layout.")?;
	let lighting_set = PersistentDescriptorSet::new
	(
		descriptor_set_allocator,
		lighting_layout.clone(),
		[
			WriteDescriptorSet::image_view(0, attachment_buffers[0].clone()),
			WriteDescriptorSet::image_view(1, attachment_buffers[1].clone()),
			WriteDescriptorSet::buffer(2, uniform_vertex_buffer.clone()),
			WriteDescriptorSet::buffer(3, uniform_lighting_buffer.clone()),
		],
	)?;

	Ok
	((
		uniform_vertex_buffer,
		uniform_lighting_buffer,
		vec!(uniform_descriptor_set, lighting_set),
	))
}

pub struct RenderPipeline
{
	device: Arc<Device>,
	queue: Arc<Queue>,
	swapchain: Arc<Swapchain>,
	framebuffers: Vec<Arc<Framebuffer>>,
	pipelines: Vec<Arc<GraphicsPipeline>>,
	uniform_vertex_buffer: Subbuffer<StandardVertexUniformData>,
	uniform_lighting_buffer: Subbuffer<StandardLightingUniformData>,
	uniform_descriptor_sets: Vec<Arc<PersistentDescriptorSet>>,
}

pub fn generate_render_pipeline(vulkano: &VulkanoContext, window: &dyn Window) -> Result<RenderPipeline, BoxErr>
{
	let device = &vulkano.device;
	let allocator = &vulkano.default_allocator;
	let render_pass = generate_render_pass(device.clone(), window)?;
	let (framebuffers, color_buffer, normal_buffer) = generate_framebuffers_from_swapchain(window.swapchain_images(), render_pass.clone(), allocator)?;
	let vertex_shader = standard_shaders::load_standard_vertex_shader(device.clone())?;
	let fragment_shader = standard_shaders::load_standard_fragment_shader(device.clone())?;
	let lighting_vertex_shader = standard_shaders::load_standard_lighting_vertex_shader(device.clone())?;
	let lighting_fragment_shader = standard_shaders::load_standard_lighting_fragment_shader(device.clone())?;
	let deferred_vertex_entry_point = vertex_shader.entry_point("main").ok_or("Could not find primary vertex entry point.")?;
	let deferred_fragment_entry_point = fragment_shader.entry_point("main").ok_or("Could not find primary fragment entry point.")?;
	let lighting_vertex_entry_point = lighting_vertex_shader.entry_point("main").ok_or("Could not find lighting vertex entry point.")?;
	let lighting_fragment_entry_point = lighting_fragment_shader.entry_point("main").ok_or("Could not find lighting fragment entry point.")?;
	let deferred_pass = Subpass::from(render_pass.clone(), 0).ok_or("render_pass: Could not find primary Subpass.")?;
	let lighting_pass = Subpass::from(render_pass.clone(), 1).ok_or("render_pass: Could not find lighting Subpass.")?;
	let graphics_pipeline = GraphicsPipeline::start()
		.vertex_input_state(crate::gfx::shaders::vertex::Vertex::per_vertex())
		.vertex_shader(deferred_vertex_entry_point, ())
		.input_assembly_state(InputAssemblyState::new())
		.viewport_state(ViewportState::viewport_fixed_scissor_irrelevant([window.viewport()]))
		.fragment_shader(deferred_fragment_entry_point, ())
		.depth_stencil_state(DepthStencilState::simple_depth_test())
		.rasterization_state(RasterizationState::new().cull_mode(CullMode::Back))
		.render_pass(deferred_pass)
		.build(vulkano.device.clone())?;
	let lighting_pipeline = GraphicsPipeline::start()
		.vertex_input_state(crate::gfx::shaders::vertex::Vertex::per_vertex())
		.vertex_shader(lighting_vertex_entry_point, ())
		.input_assembly_state(InputAssemblyState::new())
		.viewport_state(ViewportState::viewport_fixed_scissor_irrelevant([window.viewport()]))
		.fragment_shader(lighting_fragment_entry_point, ())
		.rasterization_state(RasterizationState::new().cull_mode(CullMode::Back))
		.render_pass(lighting_pass)
		.build(vulkano.device.clone())?;
	let pipelines = vec!(graphics_pipeline, lighting_pipeline);
	let
	(
		uniform_vertex_buffer,
		uniform_lighting_buffer,
		uniform_descriptor_sets,
	) = setup_uniform_layout(vulkano, pipelines.as_slice(), vec!(color_buffer, normal_buffer).as_slice())?;
	Ok(RenderPipeline
	{
		device: device.clone(),
		queue: vulkano.graphics_queue.clone(),
		swapchain: window.swapchain().clone(),
		framebuffers,
		pipelines,
		uniform_vertex_buffer,
		uniform_lighting_buffer,
		uniform_descriptor_sets,
	})
}

pub async fn execute_render_cycle(vulkano: &VulkanoContext, render_pipeline: &RenderPipeline, scene: &Scene) -> Result<bool, BoxErr>
{
	let swapchain = render_pipeline.swapchain.clone();
	let (framebuffer_index, suboptimal, acquire_swapchain) = match acquire_next_image(swapchain.clone(), None)
	{
		Err(AcquireError::OutOfDate) => return Ok(true),
		other => other
	}?;
	if suboptimal {return Ok(true);}
	let queue: &Arc<Queue> = &render_pipeline.queue;
	let framebuffer = render_pipeline
		.framebuffers
		.get(framebuffer_index as usize)
		.ok_or("Invalid framebuffer index.")?;
	let command = generate_render_cycle_command_buffer(vulkano, &render_pipeline, framebuffer.clone(), scene)?;
	{
		let correction_transform = generate_correction_transform()?;
		let projection_transform = generate_projection_transform(scene.near_plane, scene.far_plane, scene.vertical_fov, scene.aspect_ratio);
		let view_transform = generate_view_transform(scene.camera)?;
		let mut write_uniform_vertex_buffer = render_pipeline.uniform_vertex_buffer.write()?;
		write_uniform_vertex_buffer.scene_transform = SceneTransform
		{
			correction_transform,
			projection_transform,
			view_transform,
		}.combined_transform();
		let mut write_uniform_fragment_buffer = render_pipeline.uniform_lighting_buffer.write()?;
		write_uniform_fragment_buffer.ambient_light_color = *scene.ambient_light.color.as_vector3d();
		write_uniform_fragment_buffer.ambient_light_intensity = scene.ambient_light.intensity;
		write_uniform_fragment_buffer.main_light_color = *scene.main_light.color.as_vector3d();
		write_uniform_fragment_buffer.main_light_intensity = scene.main_light.intensity;
		write_uniform_fragment_buffer.main_light_direction = *scene.main_light_direction.as_vector3d();
	}
	sync::now(render_pipeline.device.clone())
		.join(acquire_swapchain)
		.then_execute(queue.clone(), command)?
		.then_swapchain_present(queue.clone(), SwapchainPresentInfo::swapchain_image_index
		(
			render_pipeline.swapchain.clone(),
			framebuffer_index,
		))
		.then_signal_fence_and_flush()?
		.await?;
	Ok(false)
}