use crate::math::{vulkano_vectors::VulkanoVectorIntegral, vectord::VectorD, transformation_matrix::TransformationMatrix, quaternion::Quaternion};

#[derive(Clone, Copy)]
pub struct ModelPlacement
{
	pub position: VectorD<VulkanoVectorIntegral, 3>,
	pub rotation: Quaternion<VulkanoVectorIntegral>,
	pub scale: VectorD<VulkanoVectorIntegral, 3>,
}

impl Default for ModelPlacement
{
	fn default() -> Self
	{
		Self
		{
			scale: VectorD::new([1.0, 1.0, 1.0]),
			rotation: Default::default(),
			position: Default::default(),
		}
	}
}

impl ModelPlacement
{
	pub fn generate_model_transforms(&self) -> (TransformationMatrix, TransformationMatrix)
	{
		let rotation = TransformationMatrix::rotation(self.rotation);
		(
			TransformationMatrix::translation(self.position)
				* rotation
				* TransformationMatrix::scaling(self.scale),
			rotation
		)
	}
}