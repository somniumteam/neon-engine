use crate::gfx::shaders::{vertex_buffer_object::VertexBufferObject, light::Light};
use crate::math::vectord::VectorD;
use crate::math::vulkano_vectors::VulkanoVectorIntegral;

use super::model_placement::ModelPlacement;

#[derive(Default)]
pub struct Scene
{
	pub models: Vec<(VertexBufferObject, ModelPlacement)>,
	pub camera: ModelPlacement,
	pub near_plane: VulkanoVectorIntegral,
	pub far_plane: VulkanoVectorIntegral,
	pub vertical_fov: VulkanoVectorIntegral,
	pub aspect_ratio: VulkanoVectorIntegral,
	pub ambient_light: Light,
	pub main_light: Light,
	pub main_light_direction: VectorD<VulkanoVectorIntegral, 3>,
}