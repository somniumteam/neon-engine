use crate::math::{vulkano_vectors::VulkanoVectorIntegral, vectord::VectorD};

#[derive(Clone, Copy)]
#[repr(C)]
pub struct Light
{
	pub color: VectorD<VulkanoVectorIntegral, 3>,
	pub intensity: VulkanoVectorIntegral,
}

impl Default for Light
{
	fn default() -> Self
	{
		Self
		{
			color: VectorD::new([1.0, 1.0, 1.0]),
			intensity: 1.0,
		}
	}
}