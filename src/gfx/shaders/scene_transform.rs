use vulkano::buffer::BufferContents;

use crate::{math::{transformation_matrix::TransformationMatrix, vulkano_vectors::VulkanoVectorIntegral}, gfx::scene::model_placement::ModelPlacement, util::box_err::BoxErr};

#[derive(BufferContents, Default, Clone, Copy)]
#[repr(C)]
pub struct SceneTransform
{
	pub correction_transform: TransformationMatrix,
	pub projection_transform: TransformationMatrix,
	pub view_transform: TransformationMatrix,
}

impl SceneTransform
{
	pub fn combined_transform(&self) -> TransformationMatrix
	{
		self.projection_transform
			* self.correction_transform
			* self.view_transform
	}
}

pub fn generate_projection_transform(near: VulkanoVectorIntegral, far: VulkanoVectorIntegral, vertical_fov: VulkanoVectorIntegral, aspect_ratio: VulkanoVectorIntegral) -> TransformationMatrix
{
	let fovy_tan_frac_2 = (vertical_fov / 2.0).tan();
	let mut ret_val = TransformationMatrix::empty();
	*ret_val.by_xy(0, 0) = 1.0 / (fovy_tan_frac_2 * aspect_ratio);
	*ret_val.by_xy(1, 1) = 1.0 / fovy_tan_frac_2;
	*ret_val.by_xy(2, 2) = (far + near) / (near - far);
	*ret_val.by_xy(3, 2) = 2.0 * far * near / (near - far);
	*ret_val.by_xy(2, 3) = -1.0;
	ret_val
}

pub fn generate_view_transform(camera_placement: ModelPlacement) -> Result<TransformationMatrix, BoxErr>
{
	let mut scale = camera_placement.scale;
	for i in 0..3
	{
		let axis = scale.dims[i];
		(if axis == 0.0 {Err("Camera scale empty axis.")} else {Ok(())})?;
		scale.dims[i] = 1.0 / axis;
	}
	let scale = TransformationMatrix::scaling(scale);
	let rotation = TransformationMatrix::rotation(camera_placement.rotation.inverse());
	let position = TransformationMatrix::translation(-camera_placement.position);
	Ok(scale * rotation * position)
}

pub fn generate_correction_transform() -> Result<TransformationMatrix, BoxErr>
{
	let mut ret_val = TransformationMatrix::empty();
	*ret_val.by_xy(1, 0) = -1.0;
	*ret_val.by_xy(2, 1) = -1.0;
	*ret_val.by_xy(0, 2) = -1.0;
	*ret_val.by_xy(3, 3) = 1.0;
	Ok(ret_val)
}