use vulkano::buffer::BufferContents;

use crate::math::transformation_matrix::TransformationMatrix;

#[derive(Default, BufferContents, Clone, Copy)]
#[repr(C)]
pub struct StandardPushConstant
{
	pub model_transform: TransformationMatrix,
	pub normal_transform: TransformationMatrix,
}