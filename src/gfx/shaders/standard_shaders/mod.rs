use std::sync::Arc;

use vulkano::{device::Device, shader::ShaderModule};

use crate::util::box_err::BoxErr;

mod standard_vertex_shader
{
	vulkano_shaders::shader!
	(
		ty: "vertex",
		path: "src/gfx/shaders/standard_shaders/standard_vertex_shader.glsl",
	);
}

mod standard_fragment_shader
{
	vulkano_shaders::shader!
	(
		ty: "fragment",
		path: "src/gfx/shaders/standard_shaders/standard_fragment_shader.glsl",
	);
}

mod standard_lighting_vertex_shader
{
	vulkano_shaders::shader!
	(
		ty: "vertex",
		path: "src/gfx/shaders/standard_shaders/standard_lighting_vertex_shader.glsl",
	);
}

mod standard_lighting_fragment_shader
{
	vulkano_shaders::shader!
	(
		ty: "fragment",
		path: "src/gfx/shaders/standard_shaders/standard_lighting_fragment_shader.glsl",
	);
}

pub fn load_standard_vertex_shader(device: Arc<Device>) -> Result<Arc<ShaderModule>, BoxErr>
{
	Ok(standard_vertex_shader::load(device)?)
}

pub fn load_standard_fragment_shader(device: Arc<Device>) -> Result<Arc<ShaderModule>, BoxErr>
{
	Ok(standard_fragment_shader::load(device)?)
}

pub fn load_standard_lighting_vertex_shader(device: Arc<Device>) -> Result<Arc<ShaderModule>, BoxErr>
{
	Ok(standard_lighting_vertex_shader::load(device)?)
}

pub fn load_standard_lighting_fragment_shader(device: Arc<Device>) -> Result<Arc<ShaderModule>, BoxErr>
{
	Ok(standard_lighting_fragment_shader::load(device)?)
}