#version 460

layout(location = 0) in vec3 color;
layout(location = 1) in vec3 normal;

layout(location = 0) out vec4 color_out;
layout(location = 1) out vec3 normal_out;

void main()
{
	color_out = vec4(color, 1.0);
	normal_out = normal;
}