#version 460

layout(input_attachment_index = 0, set = 0, binding = 0) uniform subpassInput color;
layout(input_attachment_index = 1, set = 0, binding = 1) uniform subpassInput normal;

layout(set = 0, binding = 3) uniform ambient_light
{
	vec3 ambient_light_color;
	float ambient_light_intensity;
	vec3 main_light_color;
	float main_light_intensity;
	vec3 main_light_direction;
};

layout(location = 0) in vec3 position;
layout(location = 0) out vec4 color_out;

void main()
{
	vec3 color_in = subpassLoad(color).rgb;
	vec3 normal_in = subpassLoad(normal).rgb;
	vec3 ambient_color = ambient_light_intensity * ambient_light_color;
	vec3 main_color = vec3(1.0, 1.0, 1.0);
	if(main_light_direction.x != 0.0 || main_light_direction.y != 0.0 || main_light_direction.z != 0.0)
	{
		float directional_intensity = max(dot(normal_in, main_light_direction), 0.0) * main_light_intensity;
		main_color = main_light_color * directional_intensity;
	}
	vec3 ret_val = (ambient_color + main_color) * color_in;
	color_out = vec4(ret_val, 1.0);
}