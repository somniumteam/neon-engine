#version 460

layout(location = 0) in vec3 position;

layout(set = 0, binding = 2) uniform perspective
{
	mat4 scene_transform;
};

layout(push_constant) uniform constants
{
	mat4 model_transform;
	mat4 normal_transform;
};

layout(location = 0) out vec3 position_out;

void main()
{
	gl_Position = scene_transform * model_transform * vec4(position, 1.0);
	position_out = (model_transform * vec4(position, 1.0)).xyz;
}