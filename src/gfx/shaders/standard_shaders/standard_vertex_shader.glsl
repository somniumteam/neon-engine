#version 460

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 color;
layout(location = 2) in vec3 normal;

layout(set = 0, binding = 0) uniform perspective
{
	mat4 scene_transform;
};

layout(push_constant) uniform constants
{
	mat4 model_transform;
	mat4 normal_transform;
};

layout(location = 0) out vec3 color_out;
layout(location = 1) out vec3 normal_out;

void main()
{
	gl_Position = scene_transform * model_transform * vec4(position, 1.0);
	color_out = color;
	normal_out = vec3(normal_transform * vec4(normal, 1.0));
}