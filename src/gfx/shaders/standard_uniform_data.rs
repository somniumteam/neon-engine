use vulkano::buffer::BufferContents;
use crate::math::{transformation_matrix::TransformationMatrix, vulkano_vectors::{Vector3D, VulkanoVectorIntegral}};

#[derive(Default, BufferContents, Clone, Copy)]
#[repr(C)]
pub struct StandardVertexUniformData
{
	pub scene_transform: TransformationMatrix,
}

#[derive(Default, BufferContents, Clone, Copy)]
#[repr(C)]
pub struct StandardLightingUniformData
{
	pub ambient_light_color: Vector3D,
	pub ambient_light_intensity: VulkanoVectorIntegral,
	pub main_light_color: Vector3D,
	pub main_light_intensity: VulkanoVectorIntegral,
	pub main_light_direction: Vector3D,
}