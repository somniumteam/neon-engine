use vulkano::{buffer::BufferContents, pipeline::graphics::vertex_input};
use crate::math::vulkano_vectors::Vector3D;

#[derive(vertex_input::Vertex, BufferContents, Clone, Copy, Debug, Default)]
#[repr(C)]
pub struct Vertex
{
	#[format(R32G32B32_SFLOAT)]
	pub position: Vector3D,
	#[format(R32G32B32_SFLOAT)]
	pub color: Vector3D,
	#[format(R32G32B32_SFLOAT)]
	pub normal: Vector3D,
}