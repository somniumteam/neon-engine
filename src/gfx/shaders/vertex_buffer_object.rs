use std::mem::size_of;

use vulkano::{command_buffer::{AutoCommandBufferBuilder, CommandBufferUsage, CopyBufferInfo}, buffer::{Subbuffer, Buffer, BufferCreateInfo, BufferUsage}, memory::allocator::{AllocationCreateInfo, MemoryUsage}, sync::{self, GpuFuture}};
use crate::gfx::vulkano_context::VulkanoContext;
use crate::util::box_err::BoxErr;
use super::vertex::Vertex;

pub struct VertexBufferObject
{
	pub gpu_buffer: Subbuffer<[Vertex]>,
	pub vertex_count: u32,
}

impl VertexBufferObject
{
	pub fn draw_vbo_operation<T>(&self, command_buffer_builder: &mut AutoCommandBufferBuilder<T>) -> Result<u32, BoxErr>
	{
		command_buffer_builder
			.bind_vertex_buffers(0, self.gpu_buffer.clone())
			.draw(self.vertex_count, 1, 0, 0)?;
		Ok(self.vertex_count as u32)
	}

	pub async fn upload(vulkano: &VulkanoContext, vertices: Vec<Vertex>) -> Result<VertexBufferObject, BoxErr>
	{
		let vertex_count = vertices.len();
		let upload_buffer = Buffer::from_iter
		(
			&vulkano.default_allocator,
			BufferCreateInfo {usage: BufferUsage::UNIFORM_BUFFER|BufferUsage::TRANSFER_SRC, ..Default::default()},
			AllocationCreateInfo {usage: MemoryUsage::Upload, ..Default::default()},
			vertices
		)?;
		let vertex_buffer: Subbuffer<[Vertex]> = Buffer::new_unsized
		(
			&vulkano.default_allocator,
			BufferCreateInfo {usage: BufferUsage::VERTEX_BUFFER|BufferUsage::TRANSFER_DST, ..Default::default()},
			AllocationCreateInfo {usage: MemoryUsage::DeviceOnly, ..Default::default()},
			(size_of::<Vertex>() * vertex_count).try_into().unwrap()
		)?;

		let mut command_builder = AutoCommandBufferBuilder::primary
		(
			&vulkano.command_buffer_allocator,
			vulkano.queue_family_index,
			CommandBufferUsage::OneTimeSubmit,
		)?;
		command_builder.copy_buffer
		(
			CopyBufferInfo::buffers
			(
				upload_buffer.clone(),
				vertex_buffer.clone()
			)
		)?;
		let command = command_builder.build()?;
		sync::now(vulkano.device.clone())
				.then_execute(vulkano.graphics_queue.clone(), command)?
				.then_signal_fence_and_flush()?
				.await?;
		Ok(VertexBufferObject
		{
			gpu_buffer: vertex_buffer,
			vertex_count: vertex_count as u32,
		})
	}
}