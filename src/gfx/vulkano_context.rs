use std::sync::Arc;
use vulkano::{VulkanLibrary, instance::Instance, device::{physical::PhysicalDevice, QueueFlags, Device, DeviceCreateInfo, QueueCreateInfo, Queue, DeviceExtensions}, memory::allocator::StandardMemoryAllocator, command_buffer::allocator::StandardCommandBufferAllocator, descriptor_set::allocator::StandardDescriptorSetAllocator};
pub use vulkano::instance::{InstanceCreateInfo, InstanceExtensions};
use crate::util::box_err::BoxErr;

pub struct VulkanoContext
{
	pub lib: Arc<VulkanLibrary>,
	pub instance: Arc<Instance>,
	pub physical_device: Arc<PhysicalDevice>,
	pub device: Arc<Device>,
	pub queue_family_index: u32,
	pub graphics_queue: Arc<Queue>,
	pub default_allocator: StandardMemoryAllocator,
	pub command_buffer_allocator: StandardCommandBufferAllocator,
	pub descriptor_set_allocator: StandardDescriptorSetAllocator,
}

pub fn init_vulkano(create_info: InstanceCreateInfo) -> Result<VulkanoContext, BoxErr>
{
	let lib = VulkanLibrary::new()?;
	let instance = Instance::new(lib.clone(), create_info)?;
	println!("Vulkan API Version: {}", instance.api_version());

	let device_extensions = DeviceExtensions
	{
		khr_swapchain: true,
		..Default::default()
	};

	let (physical_device, queue_family_index) = instance
		.enumerate_physical_devices()?
		.find_map(|device|
		{
			let props = device.properties();
			let (name, driver, has_render) = (props.device_name.to_owned(), props.driver_name.as_ref(), props.has_render.unwrap_or(false));
			let driver_name = match driver
			{
				None => "Unknown Driver".to_string(),
				Some(s) => s.to_owned(),
			};
			let render_text = if has_render {"Renderable"} else {"Not Renderable"};
			let supported_extensions = device.supported_extensions();
			let has_extensions = supported_extensions.contains(&device_extensions);
			let extension_text = if has_extensions {"Supported Extensions"} else {"Missing Extensions"};
			let valid = driver.is_some() && has_render && has_extensions;
			let valid_text = if valid {"VALID"} else {"INVALID"};
			println!("Enumerated device: {} on {}, {}, {}... {}", name, driver_name, render_text, extension_text, valid_text);
			if valid
			{
				let mut index: u32 = 0;
				for family in device.queue_family_properties()
				{
					let supports_graphics = family.queue_flags.contains(QueueFlags::GRAPHICS);
					let valid = supports_graphics;
					let valid_text = if valid {"VALID"} else {"INVALID"};
					println!("QueueFamily: {:?} ... {}", family, valid_text);
					if valid {return Some((device, index))}
					index += 1;
				}
				None
			}
			else {None}
		}).ok_or("Failed to enumerate devices and/or queue families.")?;

	let (device, mut queues) = Device::new
	(
		physical_device.clone(),
		DeviceCreateInfo
		{
			queue_create_infos: vec![QueueCreateInfo
			{
				queue_family_index,
				..Default::default()
			}],
			enabled_extensions: device_extensions,
			..Default::default()
		}
	)?;
	let graphics_queue = queues.next().unwrap();

	let default_allocator = StandardMemoryAllocator::new_default(device.clone());

	let command_buffer_allocator = StandardCommandBufferAllocator::new
	(
		device.clone(),
		Default::default(),
	);

	let descriptor_set_allocator = StandardDescriptorSetAllocator::new(device.clone());

	Ok(VulkanoContext
	{
		lib,
		instance,
		physical_device,
		device,
		queue_family_index,
		graphics_queue,
		default_allocator,
		command_buffer_allocator,
		descriptor_set_allocator,
	})
}