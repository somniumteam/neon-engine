// Warning is spurious.
#[allow(dead_code)]
#[derive(Debug)]
pub enum WindowEvent
{
	Close,
	Keypress(u8),
	Expose([u16; 4])
}