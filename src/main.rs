#![feature(trait_alias)]

mod gfx;
mod tech_demo;
mod math;
mod util;

use async_std::task;
use gfx::create_window::open_window_auto;
use gfx::scene::model_placement::ModelPlacement;
use gfx::scene::scene::Scene;
use gfx::shaders::light::Light;
use gfx::vulkano_context::{InstanceCreateInfo, InstanceExtensions, init_vulkano};
use gfx::create_window::{Window, WindowEvent};
use gfx::models::triangle_mesh;
use gfx::render_pipeline::{generate_render_pipeline, execute_render_cycle, RenderPipeline};
use gfx::shaders::vertex_buffer_object::VertexBufferObject;
use gfx::vulkano_context::VulkanoContext;
use math::quaternion::Quaternion;
use math::vectord::VectorD;
use math::vulkano_vectors::VulkanoVectorIntegral;
use tech_demo::buffers_demo::buffers_demo;
use tech_demo::images_demo::images_demo;
use tech_demo::graphics_demo::graphics_demo;
use tech_demo::matrices_demo::matrices_demo;
use tech_demo::quaternions_demo::quaternions_demo;
use tech_demo::vectors_demo::vectors_demo;

fn open_test_window(vulkano: &VulkanoContext, title: &str) -> (Box<dyn Window>, RenderPipeline)
{
	let window = task::block_on(async
	{
		open_window_auto(title, &vulkano)
			.await
			.expect("Could not open window.")
	});
	let pipeline = generate_render_pipeline(&vulkano, window.as_ref()).expect("Could not create render pipeline.");
	(window, pipeline)
}

fn main()
{
	let mut vulkan_create_info = InstanceCreateInfo::application_from_cargo_toml();
	vulkan_create_info.enabled_extensions = InstanceExtensions
	{
		khr_surface: true,
		khr_xcb_surface: true,
		..Default::default()
	};
	let vulkano = init_vulkano(vulkan_create_info).expect("Could not initialize Vulkan.");
	let mut models: Vec<(VertexBufferObject, ModelPlacement)> = Vec::new();
	let radius: i32 = 6;
	for x in -radius..radius
	{
		for y in -radius..radius
		{
			for z in -radius..radius
			{
				let i = models.len() as i32;
				let mut cube_mesh = triangle_mesh::TriangleMesh::cube(1.0).to_vertices();
				cube_mesh.iter_mut()
					.for_each(|v|
					{
						v.color.position[0] = (i%2) as VulkanoVectorIntegral;
						v.color.position[1] = ((i>>1)%2) as VulkanoVectorIntegral;
						v.color.position[2] = ((i>>2)%2) as VulkanoVectorIntegral;
					});
				let vbo = task::block_on(VertexBufferObject::upload(&vulkano, cube_mesh))
					.expect("Could not build cube mesh.");
				let placement = ModelPlacement
				{
					position: VectorD::new([x as f32, y as f32, z as f32]) * 1.5,
					..Default::default()
				};
				models.push((vbo, placement));
			}
		}
	}
	let mut frame = 0 as u64;
	let mut windows = vec!(open_test_window(&vulkano, "NEON Engine Test Main"));
	let mut scene = Scene
	{
		models,
		camera: ModelPlacement
		{
			position: VectorD::new([0.0, 0.0, 0.0]),
			..Default::default()
		},
		near_plane: 0.064,
		far_plane: 4096.0,
		ambient_light: Light {intensity: 0.25, ..Default::default()},
		main_light_direction: VectorD::new([1.0, 1.0, 1.0]).normalized().0 * 0.75,
		..Default::default()
	};
	while windows.len() > 0
	{
		let mut spawn_new = false;
		for (index, (ref mut window, ref render_pipeline)) in windows.iter_mut().enumerate()
		{
			frame += 1;
			let frame_interpolation = (frame as f32)/60.0;
			scene.camera.position = VectorD::new([-20.0, 0.0, 20.0]);
			scene.camera.rotation = Quaternion::from_euler(0.0, std::f32::consts::FRAC_PI_4, 0.0);
			scene.models.iter_mut().for_each(|(_, placement)|
			{
				placement.rotation = Quaternion::from_euler(frame_interpolation, 0.0, 0.0);
			});
			let next_event = window.next_event(&vulkano).expect("Failed to poll for window event.");
			if next_event.is_none()
			{
				let window_viewport = window.viewport();
				let [width, height] = window_viewport.dimensions;
				let horizontal_fov = std::f32::consts::FRAC_PI_2;
				scene.aspect_ratio = width / height;
				scene.vertical_fov = horizontal_fov / scene.aspect_ratio;
				task::block_on(execute_render_cycle(&vulkano, &render_pipeline, &scene))
					.expect("Failed to render frame.");
				continue;
			}
			let next_event = next_event.unwrap();
			if match next_event
			{
				WindowEvent::Close => true,
				WindowEvent::Keypress(keycode) =>
				{
					match keycode
					{
						24 => true,
						10 =>
						{
							buffers_demo(&vulkano).expect("buffers_demo failure:");
							false
						},
						11 =>
						{
							images_demo(&vulkano).expect("images_demo failure:");
							false
						},
						12 =>
						{
							vectors_demo().expect("vectors_demo failure");
							quaternions_demo().expect("quaternions_demo failure");
							matrices_demo().expect("matrices_demo failure");
							false
						},
						13 =>
						{
							graphics_demo(&vulkano).expect("graphics_demo failure");
							false
						},
						14 =>
						{
							spawn_new = true;
							false
						},
						keycode =>
						{
							println!("KEY: {}", keycode);
							false
						}
					}
				},
				WindowEvent::Expose(dims) =>
				{
					println!("Resized window: {:?}", dims);
					false
				},
			}
			{
				windows.remove(index);
				break;
			}
		}
		if spawn_new
		{
			let title = format!("NEON Engine #{}", windows.len());
			let title = title.as_str();
			windows.push(open_test_window(&vulkano, title));
		}
	}
}