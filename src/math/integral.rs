pub trait Integral:
	Sized +
	Copy +
	Default +
	core::fmt::Debug +
	std::ops::Add<Output = Self> +
	std::ops::AddAssign +
	std::iter::Sum +
	std::ops::Sub<Output = Self> +
	std::ops::SubAssign +
	std::ops::Mul<Output = Self> +
	std::ops::MulAssign +
	std::ops::Div<Output = Self> +
	std::ops::DivAssign +
	std::ops::Neg<Output = Self> +
	std::cmp::PartialEq
{
	fn empty() -> Self;
	fn mul_identity() -> Self;
	fn sqrt(self) -> Self;
	fn abs(self) -> Self;
	fn sin(self) -> Self;
	fn asin(self) -> Self;
	fn cos(self) -> Self;
	fn acos(self) -> Self;
	fn tan(self) -> Self;
	fn atan(self) -> Self;
}

macro_rules! IntegralSuper
{
	($itype:ident, $name:ident, $($arg:tt)*) =>
	{
		fn $name($($arg)*) -> Self {$itype::$name($($arg)*)}
	};
}

macro_rules! IntegralImpl
{
	($itype:ident) =>
	{
		impl Integral for $itype
		{
			fn empty() -> Self {0 as Self}
			fn mul_identity() -> Self {1 as Self}
			IntegralSuper!($itype, sqrt, self);
			IntegralSuper!($itype, abs, self);
			IntegralSuper!($itype, sin, self);
			IntegralSuper!($itype, asin, self);
			IntegralSuper!($itype, cos, self);
			IntegralSuper!($itype, acos, self);
			IntegralSuper!($itype, tan, self);
			IntegralSuper!($itype, atan, self);
		}
	};
}

IntegralImpl!(f32);
IntegralImpl!(f64);