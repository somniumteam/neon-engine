use super::{integral::Integral, vectord::VectorD};

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Quaternion<T: Integral>
{
	w: T,
	i: T,
	j: T,
	k: T,
}

impl<T: Integral> Default for Quaternion<T>
{
	fn default() -> Self {Self::identity()}
}

impl<T: Integral> Quaternion<T>
{
	pub fn empty() -> Self
	{
		let z = T::empty();
		Quaternion {w: z, i: z, j: z, k: z}
	}

	pub fn identity() -> Self
	{
		let z = T::empty();
		let i = T::mul_identity();
		Quaternion {w: i, i: z, j: z, k: z}
	}

	pub fn from_euler(yaw: T, pitch: T, roll: T) -> Self
	{
		let t0 = T::empty();
		let t1 = T::mul_identity();
		let t2 = t1 + t1;
		let roll = Quaternion::from_scalar_and_vector((-roll/t2).cos(), VectorD::new([(-roll/t2).sin(), t0, t0]));
		let pitch = Quaternion::from_scalar_and_vector((-pitch/t2).cos(), VectorD::new([t0, (-pitch/t2).sin(), t0]));
		let yaw = Quaternion::from_scalar_and_vector((-yaw/t2).cos(), VectorD::new([t0, t0, (-yaw/t2).sin()]));
		yaw*pitch*roll
	}

	pub fn from_scalar_and_vector(scalar: T, vector: VectorD<T, 3>) -> Quaternion<T>
	{
		let [x, y, z] = vector.dims;
		Quaternion {w: scalar, i: x, j: y, k: z}
	}

	pub fn to_scalar_and_vector(self: &Self) -> (T, VectorD<T, 3>)
	{
		(self.w, VectorD {dims: [self.i, self.j, self.k]})
	}

	pub fn norm(self: &Self) -> T
	{
		Integral::sqrt([self.w, self.i, self.j, self.k].iter().map(|n| *n**n).sum())
	}

	pub fn normalized(self: &Self) -> (Self, T)
	{
		let norm = self.norm();
		(*self / norm, norm)
	}

	pub fn conjugate(self: &Self) -> Self
	{
		let (scalar, vector) = self.to_scalar_and_vector();
		Self::from_scalar_and_vector(scalar, -vector)
	}

	pub fn inverse(self: &Self) -> Self
	{
		let norm = self.norm();
		self.conjugate() * (T::mul_identity() / (norm * norm))
	}
}

impl<I: Integral> std::ops::Mul<I> for Quaternion<I>
{
	type Output = Self;
	fn mul(self, rhs: I) -> Self::Output
	{
		Self::from_scalar_and_vector
		(
			self.w * rhs,
			VectorD{dims: [self.i, self.j, self.k]} * rhs,
		)
	}
}

impl<I: Integral> std::ops::Div<I> for Quaternion<I>
{
	type Output = Self;
	fn div(self, rhs: I) -> Self::Output
	{
		let (scalar, vector) = self.to_scalar_and_vector();
		Self::from_scalar_and_vector(scalar / rhs, vector / rhs)
	}
}

impl<I: Integral> std::ops::Mul<Self> for Quaternion<I>
{
	type Output = Self;
	fn mul(self, rhs: Self) -> Self::Output
	{
		let (scalar_b, vector_b) = self.to_scalar_and_vector();
		let (scalar_a, vector_a) = rhs.to_scalar_and_vector();
		let scalar = scalar_a * scalar_b - vector_a * vector_b;
		let vector = vector_b * scalar_a + vector_a * scalar_b + vector_a.cross_product(vector_b);
		Self::from_scalar_and_vector(scalar, vector)
	}
}