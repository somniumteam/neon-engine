use vulkano::buffer::BufferContents;
use super::{vulkano_vectors::VulkanoVectorIntegral, vectord::VectorD, quaternion::Quaternion};

macro_rules! matxy
{
	($x:expr, $y:expr) =>
	{
		$x * 4 + $y
	};
}

pub type TransformationVec = VectorD<VulkanoVectorIntegral, 3>;
pub type TransformationQuat = Quaternion<VulkanoVectorIntegral>;

#[derive(BufferContents, Debug, Clone, Copy, PartialEq, Default)]
#[repr(C)]
pub struct TransformationMatrix
{
	pub vals: [VulkanoVectorIntegral; 16]
	// Packed in column-major format.
	// Bottom row is always assumed to be 0.0, 0.0, 0.0, 1.0.
}

impl TransformationMatrix
{
	pub fn empty() -> Self
	{
		Self {vals: [0.0; 16]}
	}

	pub fn identity() -> Self
	{
		Self
		{
			vals: [1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0]
		}
	}

	pub fn scaling(vec: TransformationVec) -> Self
	{
		let mut ret_val = Self::identity();
		for i in 0..3 {ret_val.vals[matxy!(i, i)] = vec.dims[i];}
		ret_val
	}

	pub fn translation(vec: TransformationVec) -> Self
	{
		let mut ret_val = Self::identity();
		for i in 0..3 {ret_val.vals[matxy!(3, i)] = vec.dims[i];}
		ret_val
	}

	pub fn rotation(quat: TransformationQuat) -> Self
	{
		let (w, vec) = quat.to_scalar_and_vector();
		let [x, y, z] = vec.dims;
		Self
		{
			vals:
			[
				1.0-(2.0*y*y+2.0*z*z),
				2.0*x*y-2.0*z*w,
				2.0*x*z+2.0*y*w,
				0.0,
				2.0*x*y+2.0*z*w,
				1.0-(2.0*x*x+2.0*z*z),
				2.0*y*z-2.0*x*w,
				0.0,
				2.0*x*z-2.0*y*w,
				2.0*y*z+2.0*x*w,
				1.0-(2.0*x*x+2.0*y*y),
				0.0,
				0.0,
				0.0,
				0.0,
				1.0,
			]
		}
	}

	pub fn by_xy(&mut self, x: usize, y: usize) -> &mut VulkanoVectorIntegral
	{
		&mut self.vals[matxy!(x, y)]
	}
}

impl std::ops::Add<TransformationMatrix> for TransformationMatrix
{
	type Output = Self;
	fn add(self, rhs: Self::Output) -> Self::Output
	{
		TransformationMatrix
		{
			vals: self.vals.iter().zip(rhs.vals.iter())
				.map(|(a, b)| a+b)
				.collect::<Vec<VulkanoVectorIntegral>>()
				.try_into()
				.unwrap()
		}
	}
}

impl std::ops::Mul<TransformationMatrix> for TransformationMatrix
{
	type Output = Self;
	fn mul(self, rhs: Self::Output) -> Self::Output
	{
		let mut ret_val = Self::empty();
		for i in 0..4
		{
			for j in 0..4
			{
				for k in 0..4
				{
					ret_val.vals[matxy!(i, j)] += rhs.vals[matxy!(i, k)] * self.vals[matxy!(k, j)];
				}
			}
		}
		ret_val
	}
}

impl std::ops::Mul<TransformationVec> for TransformationMatrix
{
	type Output = TransformationVec;
	fn mul(self, rhs: Self::Output) -> Self::Output
	{
		let [x, y, z] = rhs.dims;
		let x_out
			= x * self.vals[matxy!(0, 0)]
			+ y * self.vals[matxy!(1, 0)]
			+ z * self.vals[matxy!(2, 0)]
			+ self.vals[matxy!(3, 0)];
		let y_out
			= x * self.vals[matxy!(0, 1)]
			+ y * self.vals[matxy!(1, 1)]
			+ z * self.vals[matxy!(2, 1)]
			+ self.vals[matxy!(3, 1)];
		let z_out
			= x * self.vals[matxy!(0, 2)]
			+ y * self.vals[matxy!(1, 2)]
			+ z * self.vals[matxy!(2, 2)]
			+ self.vals[matxy!(3, 2)];
		TransformationVec {dims: [x_out, y_out, z_out]}
	}
}