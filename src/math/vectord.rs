#![allow(dead_code)]

use super::integral::Integral;

pub trait VectorDIntegral = Integral;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(C)]
pub struct VectorD<Integral: VectorDIntegral, const DS: usize>
{
	pub dims: [Integral; DS],
}

impl<Integral: VectorDIntegral, const DS: usize> Default for VectorD<Integral, DS>
{
	fn default() -> Self {Self::empty()}
}

impl<Integral: VectorDIntegral, const DS: usize> VectorD<Integral, DS>
{
	pub fn new(dims: [Integral; DS]) -> Self
	{
		Self {dims}
	}

	pub fn empty() -> Self
	{
		Self::new([Integral::empty(); DS])
	}

	pub fn mul_identity() -> Self
	{
		Self::new([Integral::mul_identity(); DS])
	}

	pub fn sqr_magnitude(self: &Self) -> Integral
	{
		let mut ret_val = self.dims[0] * self.dims[0];
		for i in 1..DS
		{
			ret_val += self.dims[i] * self.dims[i];
		}
		ret_val
	}

	pub fn magnitude(self: &Self) -> Integral
	{
		Integral::sqrt(self.sqr_magnitude())
	}

	pub fn normalized(self: &Self) -> (Self, Integral)
	{
		let magnitude = self.magnitude();
		(*self / magnitude, magnitude)
	}

	pub fn is_empty(self: &Self) -> bool
	{
		for i in 0..DS
		{
			if self.dims[i] != Integral::empty() {return false;}
		}
		return true;
	}

	pub fn is_mul_identity(self: &Self) -> bool
	{
		for i in 0..DS
		{
			if self.dims[i] != Integral::mul_identity() {return false;}
		}
		return true;
	}

	pub fn angle_between(self: &Self, rhs: Self) -> Integral
	{
		let (a, _) = self.normalized();
		let (b, _) = rhs.normalized();
		(a*b).acos()
	}
}

impl<Integral: VectorDIntegral> VectorD<Integral, 3>
{
	pub fn cross_product(self: &Self, rhs: Self) -> Self
	{
		let [ax, ay, az] = self.dims;
		let [bx, by, bz] = rhs.dims;
		Self::new
		([
				ay*bz - az*by,
				az*bx - ax*bz,
				ax*by - ay*bx,
		])
	}
}

impl<Integral: VectorDIntegral, const DS: usize> std::ops::Mul<Self> for VectorD<Integral, DS>
{
	type Output = Integral;
	fn mul(self, rhs: Self) -> Self::Output
	{
		self.dims.iter().zip(rhs.dims.iter())
			.map(|(a, b)| *a**b)
			.sum()
	}
}

impl<Integral: VectorDIntegral, const DS: usize> std::ops::Add<Self> for VectorD<Integral, DS>
{
	type Output = Self;
	fn add(self, rhs: Self) -> Self::Output
	{
		let mut ret_val = self;
		for i in 0..DS
		{
			ret_val.dims[i] += rhs.dims[i];
		}
		ret_val
	}
}

impl<Integral: VectorDIntegral, const DS: usize> std::ops::Sub<Self> for VectorD<Integral, DS>
{
	type Output = Self;
	fn sub(self, rhs: Self) -> Self::Output
	{
		let mut ret_val = self;
		for i in 0..DS
		{
			ret_val.dims[i] -= rhs.dims[i];
		}
		ret_val
	}
}

impl<Integral: VectorDIntegral, const DS: usize> std::ops::Neg for VectorD<Integral, DS>
{
	type Output = Self;
	fn neg(self) -> Self::Output
	{
		let mut ret_val = self;
		for i in 0..DS
		{
			ret_val.dims[i] *= -Integral::mul_identity();
		}
		ret_val
	}
}

impl<Integral: VectorDIntegral, const DS: usize> std::ops::Mul<Integral> for VectorD<Integral, DS>
{
	type Output = Self;
	fn mul(self, rhs: Integral) -> Self::Output
	{
		let mut ret_val = self;
		for i in 0..DS
		{
			ret_val.dims[i] *= rhs;
		}
		ret_val
	}
}

impl<Integral: VectorDIntegral, const DS: usize> std::ops::Div<Integral> for VectorD<Integral, DS>
{
	type Output = Self;
	fn div(self, rhs: Integral) -> Self::Output
	{
		let mut ret_val = self;
		for i in 0..DS
		{
			ret_val.dims[i] /= rhs;
		}
		ret_val
	}
}