#![allow(dead_code)]

use vulkano::{pipeline::graphics::vertex_input::Vertex, buffer::BufferContents};
use super::vectord::VectorD;

pub type VulkanoVectorIntegral = f32;

#[derive(BufferContents, Vertex, Debug, Clone, Copy, PartialEq, Default)]
#[repr(C)]
pub struct Vector2D
{
	#[format(R32G32_SFLOAT)]
	pub position: [VulkanoVectorIntegral; 2]
}

impl Vector2D
{
	pub fn as_vectord(self: &Self) -> &VectorD<VulkanoVectorIntegral, 2>
	{
		unsafe {std::mem::transmute::<&Self, &VectorD<VulkanoVectorIntegral, 2>>(self)}
	}
	pub fn as_vectord_mut(self: &mut Self) -> &mut VectorD<VulkanoVectorIntegral, 2>
	{
		unsafe {std::mem::transmute::<&mut Self, &mut VectorD<VulkanoVectorIntegral, 2>>(self)}
	}
	pub fn empty() -> Self {Self {position: [0.0; 2]}}
}

impl VectorD<VulkanoVectorIntegral, 2>
{
	pub fn as_vector2d(self: &Self) -> &Vector2D
	{
		unsafe {std::mem::transmute::<&Self, &Vector2D>(self)}
	}
	pub fn as_vector2d_mut(self: &mut Self) -> &mut Vector2D
	{
		unsafe {std::mem::transmute::<&mut Self, &mut Vector2D>(self)}
	}
}

#[derive(BufferContents, Vertex, Debug, Clone, Copy, PartialEq, Default)]
#[repr(C)]
pub struct Vector3D
{
	#[format(R32G32B32_SFLOAT)]
	pub position: [VulkanoVectorIntegral; 3]
}

impl Vector3D
{
	pub fn as_vectord(self: &Self) -> &VectorD<VulkanoVectorIntegral, 3>
	{
		unsafe{std::mem::transmute::<&Self, &VectorD<VulkanoVectorIntegral, 3>>(self)}
	}
	pub fn as_vectord_mut(self: &mut Self) -> &mut VectorD<VulkanoVectorIntegral, 3>
	{
		unsafe{std::mem::transmute::<&mut Self, &mut VectorD<VulkanoVectorIntegral, 3>>(self)}
	}
	pub fn empty() -> Self {Self {position: [0.0; 3]}}
}

impl VectorD<VulkanoVectorIntegral, 3>
{
	pub fn as_vector3d(self: &Self) -> &Vector3D
	{
		unsafe {std::mem::transmute::<&Self, &Vector3D>(self)}
	}
	pub fn as_vector3d_mut(self: &mut Self) -> &mut Vector3D
	{
		unsafe {std::mem::transmute::<&mut Self, &mut Vector3D>(self)}
	}
}