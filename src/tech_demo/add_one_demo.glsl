#version 460

layout(local_size_x = 8, local_size_y = 1, local_size_z = 1) in;

layout(set = 0, binding = 0) buffer Data
{
	uint Data[];
} buf;

void main()
{
	uint i = gl_GlobalInvocationID.x;
	buf.Data[i] += 1;
}