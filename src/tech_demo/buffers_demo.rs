use std::time::Duration;
use vulkano::{buffer::{Buffer, BufferCreateInfo, BufferUsage}, memory::allocator::{AllocationCreateInfo, MemoryUsage}, command_buffer::{AutoCommandBufferBuilder, CommandBufferUsage, CopyBufferInfo}, sync::{self, GpuFuture}, pipeline::{ComputePipeline, Pipeline, PipelineBindPoint}, descriptor_set::{PersistentDescriptorSet, WriteDescriptorSet}};
use crate::gfx::vulkano_context::VulkanoContext;
use crate::util::box_err::BoxErr;

const DEFAULT_TIMEOUT: Duration = Duration::from_secs(5);
const DEMO_NUMBERS_SIZE: usize = 32;

mod add_one_shader
{
	pub const WORK_GROUP_COUNT: u32 = 4;
	vulkano_shaders::shader!
	(
		ty: "compute",
		path: "src/tech_demo/add_one_demo.glsl",
	);
}

pub fn buffers_demo(vulkano: &VulkanoContext) -> Result<(), BoxErr>
{
	let &VulkanoContext
	{
		ref device,
		ref queue_family_index,
		ref graphics_queue,
		ref default_allocator,
		ref command_buffer_allocator,
		ref descriptor_set_allocator,
		..
	} = vulkano;
	let demo_numbers = (0..(DEMO_NUMBERS_SIZE-1)).map(|i| i32::pow(2, i.try_into().unwrap()));
	let source_buffer = Buffer::from_iter
	(
		default_allocator,
		BufferCreateInfo
		{
			usage: BufferUsage::UNIFORM_BUFFER|BufferUsage::TRANSFER_SRC|BufferUsage::STORAGE_BUFFER,
			..Default::default()
		},
		AllocationCreateInfo {usage: MemoryUsage::Upload, ..Default::default()},
		demo_numbers,
	)?;
	let demo_numbers_recv = (0..(DEMO_NUMBERS_SIZE-1)).map(|_| 0 as i32);
	let destination_buffer = Buffer::from_iter
	(
		default_allocator,
		BufferCreateInfo {usage: BufferUsage::TRANSFER_DST, ..Default::default()},
		AllocationCreateInfo {usage: MemoryUsage::Download, ..Default::default()},
		demo_numbers_recv,
	)?;

	{
		let mut command_builder = AutoCommandBufferBuilder::primary
		(
			command_buffer_allocator,
			*queue_family_index,
			CommandBufferUsage::OneTimeSubmit,
		)?;
		command_builder.copy_buffer
		(
			CopyBufferInfo::buffers
			(
				source_buffer.clone(),
				destination_buffer.clone()
			)
		)?;
		let command = command_builder.build()?;

		let future = sync::now(device.clone())
			.then_execute(graphics_queue.clone(), command)?
			.then_signal_fence_and_flush()?;
		future.wait(Some(DEFAULT_TIMEOUT))?;

		let source_buffer = &*(source_buffer.read()?);
		let destination_buffer = &*(destination_buffer.read()?);
		println!("Copied data: {:?}", destination_buffer);
		assert_eq!(source_buffer, destination_buffer);
	}

	{
		let shader = add_one_shader::load(device.clone())?;
		let compute_pipeline = ComputePipeline::new
		(
			device.clone(),
			shader.entry_point("main").unwrap(),
			&(), None, |_| {}
		)?;
		let pipeline_layout = compute_pipeline.layout();

		let descriptor_set_layouts = pipeline_layout.set_layouts();
		let descriptor_set_layout = descriptor_set_layouts.get(0).ok_or("No descriptor set layouts found.")?;
		let descriptor_set = PersistentDescriptorSet::new
		(
			descriptor_set_allocator,
			descriptor_set_layout.clone(),
			[WriteDescriptorSet::buffer(0, source_buffer.clone())],
		)?;

		let mut command_builder = AutoCommandBufferBuilder::primary
		(
			command_buffer_allocator,
			*queue_family_index,
			CommandBufferUsage::OneTimeSubmit,
		)?;
		command_builder
			.bind_pipeline_compute(compute_pipeline.clone())
			.bind_descriptor_sets
			(
				PipelineBindPoint::Compute,
				pipeline_layout.clone(),
				0 as u32,
				descriptor_set.clone(),
			)
			.dispatch([add_one_shader::WORK_GROUP_COUNT, 1, 1])?;
		command_builder.copy_buffer
		(
			CopyBufferInfo::buffers
			(
				source_buffer.clone(),
				destination_buffer.clone()
			)
		)?;
		let command = command_builder.build()?;

		let future = sync::now(device.clone())
			.then_execute(graphics_queue.clone(), command)?
			.then_signal_fence_and_flush()?;
		future.wait(Some(DEFAULT_TIMEOUT))?;

		let content: Vec<u32> = destination_buffer.read()?.iter().map(|n| *n as u32).collect();
		println!("Add one results: {:?}", content);
	}

	Ok(())
}