use std::time::Duration;
use image::{ImageBuffer, Rgba};
use vulkano::{buffer::{Buffer, BufferCreateInfo, BufferUsage}, memory::allocator::{AllocationCreateInfo, MemoryUsage}, format::Format, image::{view::ImageView, StorageImage, ImageDimensions}, render_pass::{Framebuffer, FramebufferCreateInfo, Subpass}, command_buffer::{AutoCommandBufferBuilder, CommandBufferUsage, RenderPassBeginInfo, SubpassContents, CopyImageToBufferInfo}, pipeline::{graphics::{viewport::{Viewport, ViewportState}, vertex_input::Vertex, input_assembly::InputAssemblyState}, GraphicsPipeline}, sync::{self, GpuFuture}};
use box_err::BoxErr;
use crate::{gfx::vulkano_context::VulkanoContext, math::vulkano_vectors::Vector2D};
use crate::util::box_err;

const IMAGE_SAVE_DIRECTORY: &str = "./tech_demo_images";
const DEFAULT_TIMEOUT: Duration = Duration::from_secs(5);
const DEMO_IMAGE_SIZE: u16 = 8192;

mod vertex_shader
{
	vulkano_shaders::shader!
	(
		ty: "vertex",
		path: "src/tech_demo/graphics_demo_vertex.glsl",
	);
}

mod fragment_shader
{
	vulkano_shaders::shader!
	(
		ty: "fragment",
		path: "src/tech_demo/graphics_demo_fragment.glsl",
	);
}

pub fn graphics_demo(vulkano: &VulkanoContext) -> Result<(), BoxErr>
{
	let &VulkanoContext
	{
		ref device,
		queue_family_index,
		ref graphics_queue,
		ref default_allocator,
		ref command_buffer_allocator,
		..
	} = vulkano;

	let demo_points =
	[
		Vector2D {position: [-0.5, -0.5]},
		Vector2D {position: [0.0, 0.5]},
		Vector2D {position: [0.5, -0.25]}
	];

	let vertex_buffer = Buffer::from_iter
	(
		default_allocator,
		BufferCreateInfo {usage: BufferUsage::VERTEX_BUFFER, ..Default::default()},
		AllocationCreateInfo {usage: MemoryUsage::Upload, ..Default::default()},
		demo_points
	)?;

	let vertex_shader = vertex_shader::load(device.clone())?;
	let fragment_shader = fragment_shader::load(device.clone())?;

	let viewport = Viewport
	{
		origin: [0.0, 0.0],
		dimensions: [DEMO_IMAGE_SIZE.try_into()?, DEMO_IMAGE_SIZE.try_into()?],
		depth_range: 0.0..1.0,
	};

	let image = StorageImage::new
	(
		default_allocator,
		ImageDimensions::Dim2d
		{
			width: DEMO_IMAGE_SIZE.try_into()?,
			height: DEMO_IMAGE_SIZE.try_into()?,
			array_layers: 1
		},
		Format::R8G8B8A8_UNORM,
		Some(queue_family_index)
	)?;

	let render_pass = vulkano::single_pass_renderpass!
	(
		device.clone(),
		attachments:
		{
			color:
			{
				load: Clear,
				store: Store,
				format: Format::R8G8B8A8_UNORM,
				samples: 1,
			},
		},
		pass:
		{
			color: [color],
			depth_stencil: {},
		}
	)?;

	let view = ImageView::new_default(image.clone())?;
	let framebuffer = Framebuffer::new
	(
		render_pass.clone(),
		FramebufferCreateInfo
		{
			attachments: vec![view],
			..Default::default()
		}
	)?;

	let pipeline = GraphicsPipeline::start()
		.vertex_input_state(Vector2D::per_vertex())
		.vertex_shader(vertex_shader.entry_point("main").ok_or("vertex_shader: Could not find entry point.")?, ())
		.input_assembly_state(InputAssemblyState::new())
		.viewport_state(ViewportState::viewport_fixed_scissor_irrelevant([viewport]))
		.fragment_shader(fragment_shader.entry_point("main").ok_or("fragment_shader: Could not find entry point.")?, ())
		.render_pass(Subpass::from(render_pass.clone(), 0).ok_or("render_pass: Could not find Subpass.")?)
		.build(device.clone())?;

	let result_buffer = Buffer::from_iter
	(
		default_allocator,
		BufferCreateInfo {usage: BufferUsage::TRANSFER_DST, ..Default::default()},
		AllocationCreateInfo {usage: MemoryUsage::Download, ..Default::default()},
		(0..(DEMO_IMAGE_SIZE as u32).pow(2) * 4).map(|_| 0u8)
	)?;

	let mut builder = AutoCommandBufferBuilder::primary
	(
		command_buffer_allocator,
		queue_family_index,
		CommandBufferUsage::OneTimeSubmit
	)?;

	builder
		.begin_render_pass
		(
			RenderPassBeginInfo
			{
				clear_values: vec![Some([0.0,0.0,0.0,1.0].into())],
				..RenderPassBeginInfo::framebuffer(framebuffer.clone())
			},
			SubpassContents::Inline
		)?
		.bind_pipeline_graphics(pipeline.clone())
		.bind_vertex_buffers(0, vertex_buffer.clone())
		.draw(3, 1, 0, 0)?
		.end_render_pass()?
		.copy_image_to_buffer(CopyImageToBufferInfo::image_buffer(image, result_buffer.clone()))?;
	let command_buffer = builder.build()?;
	sync::now(device.clone())
		.then_execute(graphics_queue.clone(), command_buffer)?
		.then_signal_fence_and_flush()?
		.wait(Some(DEFAULT_TIMEOUT))?;

	let save_path = format!("{}/graphics_pipeline.png", IMAGE_SAVE_DIRECTORY);
	let buffer_content = result_buffer.read()?;
	ImageBuffer::<Rgba<u8>, _>::from_raw(DEMO_IMAGE_SIZE.try_into()?, DEMO_IMAGE_SIZE.try_into()?, &buffer_content[..])
		.ok_or("graphics_demo: Could not create image download buffer.")?
		.save(save_path.as_str())?;
	println!("Saved: {}", save_path);

	Ok(())
}