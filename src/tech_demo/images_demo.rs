use std::{time::Duration, path::Path, fmt::Debug, sync::Arc};
use image::{ImageBuffer, Rgba};
use vulkano::{image::{StorageImage, ImageDimensions, view::ImageView}, format::{Format, ClearColorValue}, command_buffer::{AutoCommandBufferBuilder, CommandBufferUsage, ClearColorImageInfo, CopyImageToBufferInfo, PrimaryAutoCommandBuffer}, buffer::{BufferCreateInfo, BufferUsage, Buffer, Subbuffer}, memory::allocator::{AllocationCreateInfo, MemoryUsage}, sync::{self, GpuFuture}, pipeline::{ComputePipeline, Pipeline, PipelineBindPoint}, descriptor_set::{PersistentDescriptorSet, WriteDescriptorSet}};
use crate::gfx::vulkano_context::VulkanoContext;
use crate::util::box_err::BoxErr;

use self::mandelbrot_shader::WORK_GROUP_COUNTS;

const DEFAULT_TIMEOUT: Duration = Duration::from_secs(5);
const IMAGE_DIMENSIONS: [usize; 2] = [1024, 1024];
const IMAGE_SAVE_DIRECTORY: &str = "./tech_demo_images";

mod mandelbrot_shader
{
	pub const WORK_GROUP_COUNTS: [u32; 3] = [1024/8, 1024/8, 1];
	vulkano_shaders::shader!
	(
		ty: "compute",
		path: "src/tech_demo/mandelbrot_demo.glsl",
	);
}

fn save_demo_image<S: Copy + Debug + AsRef<Path>>(path: S, image_download_buffer: Subbuffer<[u8]>) -> Result<(), BoxErr>
{
	let buffer_contents = image_download_buffer.read()?;
	let image = ImageBuffer::<Rgba<u8>, _>::from_raw
	(
		IMAGE_DIMENSIONS[0].try_into()?,
		IMAGE_DIMENSIONS[1].try_into()?,
		buffer_contents
	).ok_or("Unable to allocate image from download buffer.")?;
	image.save(path.clone())?;
	println!("Saved: {:?}", path);
	Ok(())
}

fn generate_clear_command(vulkano: &VulkanoContext, image: Arc<StorageImage>) -> Result<PrimaryAutoCommandBuffer, BoxErr>
{
	let mut clear_command = AutoCommandBufferBuilder::primary
	(
		&vulkano.command_buffer_allocator,
		vulkano.queue_family_index,
		CommandBufferUsage::OneTimeSubmit,
	)?;
	clear_command.clear_color_image
	(
		ClearColorImageInfo
		{
			clear_value: ClearColorValue::Float
			([
				10.0/255.0,
				200.0/255.0,
				1.0,
				1.0
			]),
			..ClearColorImageInfo::image(image.clone())
		}
	)?;
	Ok(clear_command.build()?)
}

fn generate_download_image_command(vulkano: &VulkanoContext, image: Arc<StorageImage>, image_download_buffer: Subbuffer<[u8]>) -> Result<PrimaryAutoCommandBuffer, BoxErr>
{
	let mut image_download_command = AutoCommandBufferBuilder::primary
	(
		&vulkano.command_buffer_allocator,
		vulkano.queue_family_index,
		CommandBufferUsage::OneTimeSubmit,
	)?;
	image_download_command.copy_image_to_buffer
	(
		CopyImageToBufferInfo::image_buffer
		(
			image.clone(),
			image_download_buffer.clone()
		)
	)?;
	Ok(image_download_command.build()?)
}

pub fn images_demo(vulkano: &VulkanoContext) -> Result<(), BoxErr>
{
	let &VulkanoContext
	{
		ref device,
		queue_family_index,
		ref graphics_queue,
		ref default_allocator,
		ref command_buffer_allocator,
		ref descriptor_set_allocator,
		..
	} = vulkano;

	std::fs::create_dir_all(IMAGE_SAVE_DIRECTORY)?;

	let image = StorageImage::new
	(
		default_allocator,
		ImageDimensions::Dim2d
		{
			width: 1024,
			height: 1024,
			array_layers: 1
		},
		Format::R8G8B8A8_UNORM,
		Some(queue_family_index)
	)?;
	let image_view = ImageView::new_default(image.clone())?;

	let image_download_buffer = Buffer::from_iter
	(
		default_allocator,
		BufferCreateInfo
		{
			usage: BufferUsage::TRANSFER_DST,
			..Default::default()
		},
		AllocationCreateInfo
		{
			usage: MemoryUsage::Download,
			..Default::default()
		},
		(0..1024*1024*4).map(|_| 0u8)
	)?;

	{
		sync::now(device.clone())
			.then_execute(graphics_queue.clone(), generate_clear_command(vulkano, image.clone())?)?
			.then_execute(graphics_queue.clone(), generate_download_image_command(vulkano, image.clone(), image_download_buffer.clone())?)?
			.then_signal_fence_and_flush()?
			.wait(Some(DEFAULT_TIMEOUT))?;
		save_demo_image
		(
			format!("{}/blank.png", IMAGE_SAVE_DIRECTORY).as_str(),
			image_download_buffer.clone()
		)?;
	}

	{
		let shader = mandelbrot_shader::load(device.clone())
			.expect("failed to create shader module");
		let compute_pipeline = ComputePipeline::new
		(
			device.clone(),
			shader.entry_point("main").ok_or("Could not find mandelbrot_demo entry point.")?,
			&(),
			None,
			|_| {},
		)?;
		let pipeline_layout = compute_pipeline.layout();
		let descriptor_set_layout = pipeline_layout
			.set_layouts()
			.get(0)
			.ok_or("Could not determine compute pipeline layout.")?;
		let descriptor_set = PersistentDescriptorSet::new
		(
			descriptor_set_allocator,
			descriptor_set_layout.clone(),
			[WriteDescriptorSet::image_view(0, image_view.clone())]
		)?;

		let mut mandelbrot_command = AutoCommandBufferBuilder::primary
		(
			command_buffer_allocator,
			queue_family_index,
			CommandBufferUsage::OneTimeSubmit,
		)?;
		mandelbrot_command
			.bind_pipeline_compute(compute_pipeline.clone())
			.bind_descriptor_sets
			(
				PipelineBindPoint::Compute,
				pipeline_layout.clone(),
				0,
				descriptor_set
			)
			.dispatch(WORK_GROUP_COUNTS)?;
		let mandelbrot_command = mandelbrot_command.build()?;

		sync::now(device.clone())
			.then_execute(graphics_queue.clone(), generate_clear_command(vulkano, image.clone())?)?
			.then_execute(graphics_queue.clone(), mandelbrot_command)?
			.then_execute(graphics_queue.clone(), generate_download_image_command(vulkano, image.clone(), image_download_buffer.clone())?)?
			.then_signal_fence_and_flush()?
			.wait(Some(DEFAULT_TIMEOUT))?;
		save_demo_image
		(
			format!("{}/mandelbrot.png", IMAGE_SAVE_DIRECTORY).as_str(),
			image_download_buffer.clone()
		)?;
	}

	Ok(())
}