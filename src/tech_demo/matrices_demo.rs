use crate::math::transformation_matrix::{TransformationMatrix, TransformationVec};
use crate::math::quaternion::Quaternion;
use crate::math::vectord::VectorD;
use crate::util::box_err::BoxErr;

pub fn matrices_demo() -> Result<(), BoxErr>
{
	let empty = TransformationMatrix::empty();
	let identity = TransformationMatrix::identity();
	println!("empty: {empty:?}, identity: {identity:?}");
	let test_vec = TransformationVec {dims: [1.0, 2.0, 3.0]};
	let scaling = TransformationMatrix::scaling(test_vec);
	let translation = TransformationMatrix::translation(test_vec);
	println!("test_vec: {test_vec:?} scaling: {scaling:?} translation: {translation:?}");
	let added_matrix = scaling + translation;
	let scaled_vec = added_matrix * test_vec;
	println!("added_matrix: {added_matrix:?} scaled_vec: {scaled_vec:?}");
	let test_quat_yaw = Quaternion::from_euler(std::f32::consts::FRAC_PI_4, 0.0, 0.0);
	let test_quat_pitch = Quaternion::from_euler(0.0, std::f32::consts::FRAC_PI_4, 0.0);
	let test_quat_roll = Quaternion::from_euler(0.0, 0.0, std::f32::consts::FRAC_PI_4);
	let rotation_matrix_yaw = TransformationMatrix::rotation(test_quat_yaw);
	let rotation_matrix_pitch = TransformationMatrix::rotation(test_quat_pitch);
	let rotation_matrix_roll = TransformationMatrix::rotation(test_quat_roll);
	let unit_x = VectorD::new([1.0, 0.0, 0.0]);
	let unit_x_yawed = rotation_matrix_yaw * unit_x;
	let unit_x_pitched = rotation_matrix_pitch * unit_x;
	let unit_z = VectorD::new([0.0, 0.0, 1.0]);
	let unit_z_rolled = rotation_matrix_roll * unit_z;
	println!("unit_x_yawed: {unit_x_yawed:?} unit_x_pitched: {unit_x_pitched:?} unit_z_rolled: {unit_z_rolled:?}");
	Ok(())
}