use crate::math::vectord::VectorD;
use crate::util::box_err::BoxErr;
use crate::math::quaternion::Quaternion;

pub fn quaternions_demo() -> Result<(), BoxErr>
{
	let empty = Quaternion::<f64>::empty();
	let identity = Quaternion::<f64>::identity();
	println!("empty: {empty:?} identity: {identity:?}");
	let test_quat_a = Quaternion::from_scalar_and_vector(1.0, VectorD::new([2.0, 3.0, 4.0]));
	let (test_quat_normalized, test_quat_norm) = test_quat_a.normalized();
	println!("test_quat_a: {test_quat_a:?} norm: {test_quat_norm:?} normalized: {test_quat_normalized:?}");
	let conjugate = test_quat_a.conjugate();
	let inverse = test_quat_a.inverse();
	println!("conjugate: {conjugate:?} inverse: {inverse:?}");
	let test_scalar = 5.0;
	let test_quat_mul = test_quat_a * test_scalar;
	let test_quat_div = test_quat_a / test_scalar;
	println!("test_scalar: {test_scalar:?} Mul: {test_quat_mul:?} Div: {test_quat_div:?}");
	let test_quat_b = Quaternion::from_scalar_and_vector(5.0, VectorD::new([6.0, 7.0, 8.0]));
	let test_quat_a_b_product = test_quat_a * test_quat_b;
	let test_quat_b_a_product = test_quat_b * test_quat_a;
	println!("test_quat_b: {test_quat_b:?} A*B {test_quat_a_b_product:?} B*A {test_quat_b_a_product:?}");
	let test_quat_roll = Quaternion::from_euler(0.0, 0.0, std::f64::consts::FRAC_PI_4);
	let test_quat_pitch = Quaternion::from_euler(0.0, std::f64::consts::FRAC_PI_4, 0.0);
	let test_quat_yaw = Quaternion::from_euler(std::f64::consts::FRAC_PI_4, 0.0, 0.0);
	println!("Yaw: {test_quat_yaw:?} Pitch: {test_quat_pitch:?} Roll: {test_quat_roll:?}");
	Ok(())
}