use crate::math::vectord::VectorD;
use crate::math::vulkano_vectors::{Vector2D, Vector3D};
use crate::util::box_err::BoxErr;

pub fn vectors_demo() -> Result<(), BoxErr>
{
	let test_vec_2 = Vector2D {position: [1.0, 2.0]};
	let test_vec_d = test_vec_2.as_vectord();
	let test_vec_2_b = test_vec_d.as_vector2d();
	let sqr_magnitude = test_vec_d.sqr_magnitude();
	let magnitude = test_vec_d.magnitude();
	println!("test_vec_2: Conversions: {test_vec_2:?} {test_vec_d:?} {test_vec_2_b:?} Magnitudes: {sqr_magnitude:?} {magnitude:?}");
	let test_vec_3 = Vector3D {position: [1.0, 2.0, 3.0]};
	let test_vec_d = test_vec_3.as_vectord();
	let test_vec_3_b = test_vec_d.as_vector3d();
	let sqr_magnitude = test_vec_d.sqr_magnitude();
	let magnitude = test_vec_d.magnitude();
	println!("test_vec_3: Conversions: {test_vec_3:?} {test_vec_d:?} {test_vec_3_b:?} Magnitudes: {sqr_magnitude:?} {magnitude:?}");
	let test_vec_4_a = VectorD::new([1.0, 2.0, 0.0, 0.0]);
	let test_vec_4_b = VectorD::new([0.0, 0.0, 3.0, 4.0]);
	let test_vec_4_add = test_vec_4_a + test_vec_4_b;
	let test_vec_4_sub = test_vec_4_a - test_vec_4_b;
	println!("test_vec_4: Operands: {test_vec_4_a:?} {test_vec_4_b:?}");
	println!("test_vec_4: Add: {test_vec_4_add:?} Sub: {test_vec_4_sub:?}");
	let test_vec_4_mul = test_vec_4_add * 2.0;
	let test_vec_4_div = test_vec_4_add / 2.0;
	println!("test_vec_4: Mul: {test_vec_4_mul:?} Div: {test_vec_4_div:?}");
	let test_vec_4_empty = VectorD::<f32, 4>::empty();
	let test_vec_4_identity = VectorD::<f32, 4>::mul_identity();
	println!("test_vec_4: Empty: {test_vec_4_empty:?} Identity: {test_vec_4_identity:?}");
	let (test_vec_4_normalized, magnitude) = test_vec_4_identity.normalized();
	println!("test_vec_4: Normalized: {test_vec_4_normalized:?} Magnitude: {magnitude:?}");
	let test_vec_3 = *test_vec_3.as_vectord();
	let test_vec_3_dot = test_vec_3*test_vec_3;
	println!("test_vec_3: {test_vec_3:?} Dot: {test_vec_3_dot:?}");
	let test_vec_3_a = VectorD::new([1.0, 1.0, 0.0]);
	let test_vec_3_b = VectorD::new([0.0, 1.0, 1.0]);
	let test_vec_3_cross = test_vec_3_a.cross_product(test_vec_3_b);
	println!("test_vec_3: {test_vec_3_a:?} {test_vec_3_b:?} Cross: {test_vec_3_cross:?}");
	let angle_a_b = test_vec_3_a.angle_between(test_vec_3_b);
	let angle_a_cross = test_vec_3_a.angle_between(test_vec_3_cross);
	let angle_b_cross = test_vec_3_b.angle_between(test_vec_3_cross);
	println!("Angle A/B: {angle_a_b:?} Angle A/C: {angle_a_cross:?} Angle B/C: {angle_b_cross:?}");
	Ok(())
}